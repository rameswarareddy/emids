package com.emids.healthinsurance.dto;

import java.util.Arrays;
import java.util.List;

import com.emids.healthinsurance.common.CurrentHealth;
import com.emids.healthinsurance.common.Gender;
import com.emids.healthinsurance.common.Habits;

public class ApplicantDto {
	String name;
	Gender gender;
	int age;
	List<CurrentHealth> currHealth;
	List<Habits> habits;

	public ApplicantDto() {

	}

	public ApplicantDto(String name, Gender gender, int age, List<CurrentHealth> currHealth, List<Habits> habits) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currHealth = currHealth;
		this.habits = habits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Habits> getHabits() {
		return habits;
	}

	public void setHabits(List<Habits> habits) {
		this.habits = habits;
	}

	public List<CurrentHealth> getCurrHealth() {
		return currHealth;
	}

	public void setCurrHealth(List<CurrentHealth> currHealth) {
		this.currHealth = currHealth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "ApplicantDto [name=" + name + ", gender=" + gender + ", age=" + age + ", currHealth=" + currHealth
				+ ", habits=" + habits + "]";
	}


}
