package com.emids.healthinsurance.common;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface Constants {
	Integer basePremiam=5000;
	ObjectMapper mapper = new ObjectMapper();
	String habitsFilePath="/home/emidstest05/ram-workspace-DO_ NOT_DELETE/HealthInsurance/src/Habits.json";
	String currentHealthFilePath="/home/emidstest05/ram-workspace-DO_ NOT_DELETE/HealthInsurance/src/CurrentHealth.json";
	String ageBasePriceFilePath="/home/emidstest05/ram-workspace-DO_ NOT_DELETE/HealthInsurance/src/AgeBasedPrice.json";
	String pricePointsFilePath="/home/emidstest05/ram-workspace-DO_ NOT_DELETE/HealthInsurance/src/PricePoints.json";
}
