package com.emids.healthinsurance.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Gender {
	@JsonProperty("Male")MALE("Male"),
	@JsonProperty("Female")FEMALE("Female"),
	@JsonProperty("Others")OTHERS("Others");
	private String gender;

	Gender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}
}
