package com.emids.healthinsurance.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Habits {
	@JsonProperty("Smoking")SMOKING("Smoking"), 
	@JsonProperty("Alcohol")ALCOHOL("Alcohol"), 
	@JsonProperty("Daily exercise")DAILY_EXERCISE("Daily exercise"), 
	@JsonProperty("Drugs")DRUGS("Drugs");

	private String habit;

	Habits(String habit) {
		this.habit = habit;
	}

	public String getHabit() {
		return habit;
	}

}
