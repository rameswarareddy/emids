package com.emids.healthinsurance.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum CurrentHealth {
	@JsonProperty("Hypertension")HYPERTENSION("Hypertension"),
	@JsonProperty("Blood pressure")BLOOD_PRESSURE("Blood pressure"),
	@JsonProperty("Blood sugar")BLOOD_SUGAR("Blood sugar"), 
	@JsonProperty("Overweight")OVERWEIGHT("Overweight");

	private String currentHealth;

	CurrentHealth(String currentHealth) {
		this.currentHealth = currentHealth;
	}

	public String getCurrentHealth() {
		return currentHealth;
	}

}
