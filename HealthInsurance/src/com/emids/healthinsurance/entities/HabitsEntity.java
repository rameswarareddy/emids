package com.emids.healthinsurance.entities;

import java.io.Serializable;
import java.util.List;

import com.emids.healthinsurance.common.Habits;

public class HabitsEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;
	List<Habits> good;
	List<Habits> bad;
	public List<Habits> getGood() {
		return good;
	}
	public void setGood(List<Habits> good) {
		this.good = good;
	}
	public List<Habits> getBad() {
		return bad;
	}
	public void setBad(List<Habits> bad) {
		this.bad = bad;
	}
	@Override
	public String toString() {
		return "HabitsEntity [good=" + good + ", bad=" + bad + "]";
	}
	
	
}
