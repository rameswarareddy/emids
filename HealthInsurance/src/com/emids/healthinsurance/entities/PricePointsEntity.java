package com.emids.healthinsurance.entities;

import java.util.Map;

import com.emids.healthinsurance.common.Gender;

public class PricePointsEntity {
	private double badHabits;
	private Map<Gender, Integer> gender;
	private double currHealth;
	private double goodHabits;
	public double getGoodHabits() {
		return goodHabits;
	}
	public void setGoodHabits(double goodHabits) {
		this.goodHabits = goodHabits;
	}
	public double getBadHabits() {
		return badHabits;
	}
	public void setBadHabits(double badHabits) {
		this.badHabits = badHabits;
	}
	
	public double getCurrHealth() {
		return currHealth;
	}
	public void setCurrHealth(double currHealth) {
		this.currHealth = currHealth;
	}
	public Map<Gender, Integer> getGender() {
		return gender;
	}
	public void setGender(Map<Gender, Integer> gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "PricePointsEntity [badHabits=" + badHabits + ", gender=" + gender + ", currHealth=" + currHealth
				+ ", goodHabits=" + goodHabits + "]";
	}
	
	
}
