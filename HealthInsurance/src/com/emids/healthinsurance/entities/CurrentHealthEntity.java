package com.emids.healthinsurance.entities;

import java.util.List;

import com.emids.healthinsurance.common.CurrentHealth;

public class CurrentHealthEntity {
	List<CurrentHealth> currHealth;

	public List<CurrentHealth> getCurrHealth() {
		return currHealth;
	}

	public void setCurrHealth(List<CurrentHealth> currHealth) {
		this.currHealth = currHealth;
	}

	@Override
	public String toString() {
		return "CurrentHealthEntity [currHealth=" + currHealth + "]";
	}
	
}
