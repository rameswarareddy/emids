package com.emids.healthinsurance.dao;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.emids.healthinsurance.entities.AgeBasedPrice;
import com.emids.healthinsurance.entities.CurrentHealthEntity;
import com.emids.healthinsurance.entities.HabitsEntity;
import com.emids.healthinsurance.entities.PricePointsEntity;
import com.fasterxml.jackson.databind.ObjectMapper;

public class InsuranceCalculatorDaoImpl implements InsuranceCalculatorDao {
	private HabitsEntity habits = null;
	{
		ObjectMapper mapper = new ObjectMapper();
		// JSON from file to Object
		try {
			habits = mapper.readValue(new File(habitsFilePath), HabitsEntity.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private CurrentHealthEntity currentHealthEntity;
	{
		try {
			currentHealthEntity = mapper.readValue(new File(currentHealthFilePath), CurrentHealthEntity.class);
			//System.out.println(currentHealthEntity);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private List<AgeBasedPrice> ageBasedPrices = new ArrayList<AgeBasedPrice>();
	{
		try {
			ageBasedPrices = mapper.readValue(new File(ageBasePriceFilePath),
					mapper.getTypeFactory().constructCollectionType(List.class, AgeBasedPrice.class));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private PricePointsEntity pricePoints = null;
	{
		try {
			pricePoints = mapper.readValue(new File(pricePointsFilePath), PricePointsEntity.class);
			//System.out.println(pricePoints);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		InsuranceCalculatorDaoImpl t = new InsuranceCalculatorDaoImpl();
	}

	public HabitsEntity getHabits() {
		return habits;
	}

	public void setHabits(HabitsEntity habits) {
		this.habits = habits;
	}

	public List<AgeBasedPrice> getAgeBasedPrices() {
		return ageBasedPrices;
	}

	public void setAgeBasedPrices(List<AgeBasedPrice> ageBasedPrices) {
		this.ageBasedPrices = ageBasedPrices;
	}

	public PricePointsEntity getPricePoints() {
		return pricePoints;
	}

	public void setPricePoints(PricePointsEntity pricePoints) {
		this.pricePoints = pricePoints;
	}

	public CurrentHealthEntity getCurrentHealthEntity() {
		return currentHealthEntity;
	}

	public void setCurrentHealthEntity(CurrentHealthEntity currentHealthEntity) {
		this.currentHealthEntity = currentHealthEntity;
	}

}
