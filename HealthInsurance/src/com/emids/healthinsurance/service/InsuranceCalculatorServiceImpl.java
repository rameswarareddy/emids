package com.emids.healthinsurance.service;

import java.util.List;
import java.util.Map;

import com.emids.healthinsurance.common.CurrentHealth;
import com.emids.healthinsurance.common.Gender;
import com.emids.healthinsurance.common.Habits;
import com.emids.healthinsurance.dao.InsuranceCalculatorDaoImpl;
import com.emids.healthinsurance.dto.ApplicantDto;
import com.emids.healthinsurance.entities.AgeBasedPrice;
import com.emids.healthinsurance.entities.CurrentHealthEntity;
import com.emids.healthinsurance.entities.HabitsEntity;
import com.emids.healthinsurance.entities.PricePointsEntity;

public class InsuranceCalculatorServiceImpl implements InsuranceCalculatorService {
	InsuranceCalculatorDaoImpl dao = new InsuranceCalculatorDaoImpl();

	@Override
	public double getApplicantInsuranceAmt(ApplicantDto applicantDto) {

		double insAmt = dao.basePremiam;

		// 1.Based on age
		insAmt = applyAgePolacy(applicantDto, insAmt);

		// 2.Gender based
		insAmt = applyGenderPlacy(applicantDto, insAmt);

		// 3.Based On Current Health
		insAmt = applyCurrHealthPolacy(applicantDto, insAmt);

		// 4.Based on Habits
		insAmt = applyHabitsPolacy(applicantDto, insAmt);

		return insAmt;
	}

	@Override
	public double applyAgePolacy(ApplicantDto applicantDto, double insAmt) {
		int age = applicantDto.getAge();
		for (AgeBasedPrice ageBasedPrice : dao.getAgeBasedPrices()) {
			if (age >= ageBasedPrice.getMax() || (age > ageBasedPrice.getMin() && age <= ageBasedPrice.getMax())) {
				double ageInsAmt = (insAmt * ageBasedPrice.getVal()) / 100;
				insAmt += ageInsAmt;
			}
		}
		return insAmt;
	}

	@Override
	public double applyGenderPlacy(ApplicantDto applicantDto, double insAmt) {
		PricePointsEntity pricePoints = dao.getPricePoints();
		Map<Gender, Integer> genderPrciePoint = pricePoints.getGender();
		if (genderPrciePoint != null && genderPrciePoint.containsKey(applicantDto.getGender())) {
			double genderInsAmt = (insAmt * genderPrciePoint.get(applicantDto.getGender())) / 100;
			insAmt += genderInsAmt;
		}
		return insAmt;
	}

	@Override
	public double applyCurrHealthPolacy(ApplicantDto applicantDto, double insAmt) {
		CurrentHealthEntity habits = dao.getCurrentHealthEntity();
		PricePointsEntity pricePoints = dao.getPricePoints();
		List<CurrentHealth> currHealthArr = applicantDto.getCurrHealth();
		for (CurrentHealth currHealth : currHealthArr) {
			if (habits.getCurrHealth().contains(currHealth)) {
				double currHealthInsAmt = (insAmt * pricePoints.getCurrHealth()) / 100;
				insAmt += currHealthInsAmt;
			}
		}
		return insAmt;
	}

	@Override
	public double applyHabitsPolacy(ApplicantDto applicantDto, double ins_amt) {
		PricePointsEntity pricePoints = dao.getPricePoints();
		HabitsEntity habits = dao.getHabits();
		List<Habits> habitsArr = applicantDto.getHabits();
		double habitsInsVal = 0;
		for (Habits habit : habitsArr) {
			if (habits.getGood().contains(habit)) {
				habitsInsVal = habitsInsVal + pricePoints.getGoodHabits();
			} else if (habits.getBad().contains(habit)) {
				habitsInsVal = habitsInsVal + pricePoints.getBadHabits();
			}
		}
		if (habitsInsVal != 0) {
			double habitInsAmt = (ins_amt * habitsInsVal) / 100;
			ins_amt += habitInsAmt;
		}
		return ins_amt;
	}
}
