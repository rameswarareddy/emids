package com.emids.healthinsurance.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AgePolacyTest.class, CurrHealthPolacyTest.class, GenderPolacyTest.class, HabitsPolacyTest.class,
		HealtInsuranceTest.class })
public class HealthInsuranceTestSuite {

}