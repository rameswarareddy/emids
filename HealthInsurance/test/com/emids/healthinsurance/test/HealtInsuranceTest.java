package com.emids.healthinsurance.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.emids.healthinsurance.common.CurrentHealth;
import com.emids.healthinsurance.common.Gender;
import com.emids.healthinsurance.common.Habits;
import com.emids.healthinsurance.dto.ApplicantDto;
import com.emids.healthinsurance.service.InsuranceCalculatorService;
import com.emids.healthinsurance.service.InsuranceCalculatorServiceImpl;

import junit.framework.Assert;

public class HealtInsuranceTest {

	public static ApplicantDto applicantDto = null;
	public static InsuranceCalculatorService insuranceCalculatorService = null;

	@BeforeClass
	public static void setup() {
		insuranceCalculatorService = new InsuranceCalculatorServiceImpl();
	}

	@Before
	public void loadApplicant() {
		List<Habits> habits = new ArrayList<Habits>();
		habits.add(Habits.ALCOHOL);
		habits.add(Habits.DAILY_EXERCISE);
		List<CurrentHealth> currentHealths = new ArrayList<CurrentHealth>();
		currentHealths.add(CurrentHealth.OVERWEIGHT);
		applicantDto = new ApplicantDto("Norman Gomes", Gender.MALE, 34, currentHealths, habits);
	}

	@After
	public void distuctApplicant() {
		applicantDto = null;
	}

	@Test
	public void getInsuranceAmtTest() {
		Double insAmt = insuranceCalculatorService.getApplicantInsuranceAmt(applicantDto);
		Assert.assertEquals(6856.0, Math.ceil(insAmt));
	}

	@Test
	public void getInsuranceAmtTestTwos() {
		List<Habits> habits = new ArrayList<Habits>();
		habits.add(Habits.ALCOHOL);

		List<CurrentHealth> currentHealths = new ArrayList<CurrentHealth>();
		currentHealths.add(CurrentHealth.OVERWEIGHT);

		applicantDto = new ApplicantDto("Norman Gomes", Gender.MALE, 34, currentHealths, habits);

		Double insAmt = insuranceCalculatorService.getApplicantInsuranceAmt(applicantDto);
		Assert.assertEquals(7062.0, Math.ceil(insAmt));
	}

	@AfterClass
	public static void end() {
		insuranceCalculatorService = null;
	}
}
