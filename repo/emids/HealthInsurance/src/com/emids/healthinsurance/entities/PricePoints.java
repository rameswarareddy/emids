package com.emids.healthinsurance.entities;

import java.util.Map;

public class PricePoints {
	private double badHabits;
	private Map<String, Integer> gender;
	private double currHealth;
	private double goodHabits;
	public double getGoodHabits() {
		return goodHabits;
	}
	public void setGoodHabits(double goodHabits) {
		this.goodHabits = goodHabits;
	}
	public double getBadHabits() {
		return badHabits;
	}
	public void setBadHabits(double badHabits) {
		this.badHabits = badHabits;
	}
	public Map<String, Integer> getGender() {
		return gender;
	}
	public void setGender(Map<String, Integer> gender) {
		this.gender = gender;
	}
	public double getCurrHealth() {
		return currHealth;
	}
	public void setCurrHealth(double currHealth) {
		this.currHealth = currHealth;
	}
	@Override
	public String toString() {
		return "PricePoints [badHabits=" + badHabits + ", gender=" + gender + ", currHealth=" + currHealth
				+ ", goodHabits=" + goodHabits + "]";
	}
	
	
}
