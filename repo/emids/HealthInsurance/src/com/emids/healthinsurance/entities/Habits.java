package com.emids.healthinsurance.entities;

import java.io.Serializable;
import java.util.List;

public class Habits implements Serializable{
	List<String> good;
	List<String> bad;
	List<String> curr_health;
	public List<String> getGood() {
		return good;
	}
	public void setGood(List<String> good) {
		this.good = good;
	}
	public List<String> getBad() {
		return bad;
	}
	public void setBad(List<String> bad) {
		this.bad = bad;
	}
	public List<String> getCurr_health() {
		return curr_health;
	}
	public void setCurr_health(List<String> curr_health) {
		this.curr_health = curr_health;
	}
	@Override
	public String toString() {
		return "Habits [good=" + good + ", bad=" + bad + ", curr_health=" + curr_health + "]";
	}
	
	
}
