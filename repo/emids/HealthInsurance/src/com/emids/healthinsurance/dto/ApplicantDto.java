package com.emids.healthinsurance.dto;

import java.util.Arrays;

public class ApplicantDto {
	String name;
	String gender;
	int age;
	String[] curr_health;
	String[] habits;

	public ApplicantDto() {

	}

	public ApplicantDto(String name, String gender, int age, String[] curr_health, String[] habits) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.curr_health = curr_health;
		this.habits = habits;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String[] getCurr_health() {
		return curr_health;
	}

	public void setCurr_health(String[] curr_health) {
		this.curr_health = curr_health;
	}

	public String[] getHabits() {
		return habits;
	}

	public void setHabits(String[] habits) {
		this.habits = habits;
	}

	@Override
	public String toString() {
		return "ApplicantDto [name=" + name + ", gender=" + gender + ", age=" + age + ", curr_health="
				+ Arrays.toString(curr_health) + ", habits=" + Arrays.toString(habits) + "]";
	}

	
}
