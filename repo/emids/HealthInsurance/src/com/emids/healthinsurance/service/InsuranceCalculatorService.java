package com.emids.healthinsurance.service;

import com.emids.healthinsurance.dto.ApplicantDto;

public interface InsuranceCalculatorService{
	double getApplicantInsuranceAmt(ApplicantDto applicantDto);

	double applyAgePolacy(ApplicantDto applicantDto, double ins_amt);

	double applyGenderPlacy(ApplicantDto applicantDto, double ins_amt);

	double applyCurrHealthPolacy(ApplicantDto applicantDto, double ins_amt);

	double applyHabitsPolacy(ApplicantDto applicantDto, double ins_amt);
}
