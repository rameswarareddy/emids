package com.emids.healthinsurance.test;

import org.junit.Test;

import com.emids.healthinsurance.dto.ApplicantDto;
import com.emids.healthinsurance.service.InsuranceCalculatorService;
import com.emids.healthinsurance.service.InsuranceCalculatorServiceImpl;

import junit.framework.Assert;

public class GenderPolacyTest {
	InsuranceCalculatorService insuranceCalculatorService = new InsuranceCalculatorServiceImpl();

	@Test
	public void applyGenderPlacyTest() {
		ApplicantDto applicantDto = new ApplicantDto("Norman Gomes", "Male", 34, new String[] { "Overweight" },
				new String[] { "Alcohol", "Daily exercise" });
		Double insAmt = insuranceCalculatorService.applyGenderPlacy(applicantDto, 5000);
		Assert.assertEquals(5100.0, insAmt);
	}

	public static void main(String[] args) {
		GenderPolacyTest t = new GenderPolacyTest();
		t.applyGenderPlacyTest();
	}
}
