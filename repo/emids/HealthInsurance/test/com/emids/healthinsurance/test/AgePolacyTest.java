package com.emids.healthinsurance.test;

import org.junit.Test;

import com.emids.healthinsurance.dto.ApplicantDto;
import com.emids.healthinsurance.service.InsuranceCalculatorService;
import com.emids.healthinsurance.service.InsuranceCalculatorServiceImpl;

import junit.framework.Assert;

public class AgePolacyTest {

	InsuranceCalculatorService insuranceCalculatorService = new InsuranceCalculatorServiceImpl();
	@Test
	public void applyAgePolacyTestOne() {
		ApplicantDto applicantDto = new ApplicantDto("Norman Gomes", "Male", 34, new String[] { "Overweight" },
				new String[] { "Alcohol", "Daily exercise" });
		Double insAmt = insuranceCalculatorService.applyAgePolacy(applicantDto,5000);
		//System.out.println(insAmt);
		Assert.assertEquals(6655.0, insAmt);
	}

	@Test
	public void applyAgePolacyTestTwo() {
		ApplicantDto applicantDto = new ApplicantDto("Norman Gomes", "Male", 19, new String[] { "Overweight" },
				new String[] { "Alcohol", "Daily exercise" });
		Double insAmt = insuranceCalculatorService.applyAgePolacy(applicantDto,5000);
		Assert.assertEquals(5500.0, insAmt);
	}
	
	public static void main(String[] args) {
		AgePolacyTest t=new AgePolacyTest();
		t.applyAgePolacyTestOne();
	}
}
