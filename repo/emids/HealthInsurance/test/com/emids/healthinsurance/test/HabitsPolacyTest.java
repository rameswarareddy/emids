package com.emids.healthinsurance.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.emids.healthinsurance.dto.ApplicantDto;
import com.emids.healthinsurance.service.InsuranceCalculatorService;
import com.emids.healthinsurance.service.InsuranceCalculatorServiceImpl;

import junit.framework.Assert;

public class HabitsPolacyTest {
	InsuranceCalculatorService insuranceCalculatorService = new InsuranceCalculatorServiceImpl();
	@Test
	public void applyHabitsPolacyTest() {
		ApplicantDto applicantDto = new ApplicantDto("Norman Gomes", "Male", 34, new String[] { "Overweight" },
				new String[] { "Alcohol", "Daily exercise" });
		Double insAmt = insuranceCalculatorService.applyHabitsPolacy(applicantDto, 5000);
		Assert.assertEquals(5000.0, insAmt);
		//System.out.println(insAmt);
	}

	public static void main(String[] args) {
		HabitsPolacyTest t = new HabitsPolacyTest();
		t.applyHabitsPolacyTest();
	}

}
