package com.emids.healthinsurance.test;
import org.junit.Test;

import com.emids.healthinsurance.dto.ApplicantDto;
import com.emids.healthinsurance.service.InsuranceCalculatorService;
import com.emids.healthinsurance.service.InsuranceCalculatorServiceImpl;

import junit.framework.Assert;

public class HealtInsuranceTest {

	@Test
	public void getInsuranceAmtTest() {
		InsuranceCalculatorService insuranceCalculatorService = new InsuranceCalculatorServiceImpl();
		ApplicantDto applicantDto = new ApplicantDto("Norman Gomes", "Male", 34, new String[] { "Overweight" },
				new String[] { "Alcohol", "Daily exercise" });
		Double ins_amt = insuranceCalculatorService.getApplicantInsuranceAmt(applicantDto);
		Assert.assertEquals(6856.0, Math.ceil(ins_amt));
		//System.out.println(ins_amt);
	}

	
	
	
	public static void main(String[] args) {
		InsuranceCalculatorService insuranceCalculatorService = new InsuranceCalculatorServiceImpl();
		ApplicantDto applicantDto = new ApplicantDto("Norman Gomes", "Male", 34, new String[] { "Overweight" },
				new String[] { "Alcohol", "Daily exercise" });
		Double ins_amt = insuranceCalculatorService.getApplicantInsuranceAmt(applicantDto);
		System.out.println(Math.ceil(ins_amt));
	}
}
